const express = require('express');
const process = require('process')
const app = express();

const PORT=8000


app.use('/', (req,res,next)=>{
    return res.send('<h1>Hello World!!</h1>')
})


app.listen(PORT,()=>{
    console.log(`Listening on the server ${PORT}`)
    setTimeout(()=>{
        process.exit(1)
    },2000)
})