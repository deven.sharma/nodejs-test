function applyBuildPack() {
    docker run \
  --env SOURCE_CODE="$PWD" \
  --volume "$PWD":/code \
  --volume /var/run/docker.sock:/var/run/docker.sock \
  registry.gitlab.com/gitlab-org/ci-cd/codequality:${VERSION:-latest} /code

}
function runStaticAnalysis()
{
  docker run \
  --interactive --tty --rm \
  --volume "$PWD":/code \
  --volume /var/run/docker.sock:/var/run/docker.sock \
  registry.gitlab.com/gitlab-org/security-products/sast:${VERSION:-latest} /app/bin/run /code

}

function checkForLeaks()
{
    docker pull zricethezav/gitleaks
    docker run zricethezav/gitleaks -r=https://gitlab.com/deven.sharma/nodejs-test.git
}

applyBuildPack  
runStaticAnalysis
checkForLeaks